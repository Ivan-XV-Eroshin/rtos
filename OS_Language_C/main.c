#include <avr/interrupt.h>
#include <avr/iom128.h>
#include <stddef.h>
#include <stdlib.h>

#define CPU_FREQUENCY_MHZ 8 //TODO расчитать для всех возможных значений начальную позицию
#define TASK_QUEUE_SIZE 5 //размер очереди TASK_QUEUE_SIZE + 1 (см. создание main_task_queue)

#if CPU_FREQUENCY_MHZ==8
#define SYSTEM_TIMER_START_VALUE 130 //использовать с восьмибитными таймерами
#endif

typedef uint16_t TIME;

typedef void (*TPTR)(void);

//создание очереди задач, в которую будут помещаться задачи
static TPTR main_task_queue[TASK_QUEUE_SIZE + 1];

//создание структуры задачи, передаваемой обработчику таймера
struct Task {
	struct Task* next_task;
	TPTR task_handler;
	TIME wait_time;
};
typedef struct Task Task;
/////////////////////////////////////////////
//обязательно инициализировать элементом!!!//
/////////////////////////////////////////////
Task* list_head;

void Idle();
void initSystem();
void initRTOS();
void initMainQueue();
void InitTasklist();
Task* createTask(TPTR task_handler, TIME wait_time);
void addTaskToTasklist(Task* task);
void updateTaskTime(TPTR task_handler, TIME new_time);
void insertTask(Task* task);
void TaskDispatcher();
void setTask(TPTR task_handler);
void TimerService();

ISR(TIMER2_OVF_vect){
	TCNT2 = 130;
	TimerService();
}

void initRTOS(){
	TCNT2 = 130;
	TIMSK |= (1 << TOIE2);//разрешение прерываний таймера по сравнению
	TCCR2 |= (1<<CS21) | (1<<CS20); //запуск таймера с предделителем 64
	sei();
}

//изменяет глобальную переменную
void InitTasklist () {
	list_head = malloc(sizeof(struct Task));
	if(list_head){
		list_head->next_task = NULL;
		list_head->task_handler = Idle;
		list_head->wait_time = 1;
	}
}

Task* createTask(TPTR task_handler,TIME wait_time){
	struct Task* new_task = malloc(sizeof(struct Task));
	if (new_task) {
		new_task->next_task = NULL;
		new_task->task_handler = task_handler;
		new_task->wait_time = wait_time;
	}
	return new_task;
}

void updateTaskTime(TPTR task_handler, TIME new_time){
	struct Task* current_task = list_head;
	while(current_task != NULL){
		if(current_task->task_handler == task_handler){
			current_task->wait_time = new_time;
			return;
		}
		current_task = current_task->next_task;
	}
}

#ifdef DEBUG
//изменяет глобальную переменную
//функция упорядоченной вставки задачи в лист задач
void insertTask(Task* task) {
	Task* current_task = list_head;
	Task* prev_task = NULL;
	/*
		цикл до тех пор, пока не программа не дойдет до конечного элемента
		доходит до конечного элемента в том случае, если поставленная задача имеет наибольшую 
		задержку до исполнения в списке процессов
	*/
	do {
		/*
			Если время у текущей задачи больше, чем у той, которую надо вставить
			происходит вставка с проверкой на то, стоит ли элемент на первой позиции и выходом из функции
			В противном случае - переход к следующему элементу
		*/
		if (current_task->wait_time > task->wait_time || current_task->next_task == NULL) {
			task->next_task = current_task;
			if(list_head == current_task)
				list_head = task;
			else
				prev_task->next_task = task;
			return;
		}
		else {
			prev_task = current_task;
			current_task = current_task->next_task;
		}
	} while(current_task != NULL);
}
#endif1

inline void Idle(){
	
}

inline void TaskDispatcher (){
	uint8_t	index = 0;
	cli();
	TPTR task = main_task_queue[0];
	if (task == NULL) {
		sei();
		return;
	} else if (task == Idle) {
		sei();
		(Idle)();
	} else {
		for(index = 0; index != TASK_QUEUE_SIZE; index++) {
			main_task_queue[index] = main_task_queue[index+1];
		}
		main_task_queue[TASK_QUEUE_SIZE] = Idle;
		sei();
		(task)();// Переходим к задаче
	}
}

void addTaskToTasklist(Task* task){
	task->next_task = list_head;
	list_head = task;
}

inline void setTask(TPTR task_handler){
	uint8_t index = 0;
	while (main_task_queue[index] != Idle && main_task_queue[index] != NULL){
		index++;
		if(index == TASK_QUEUE_SIZE + 1) {
			return;
		}
 	}
	main_task_queue[index] = task_handler;
}

inline void TimerService() {
	struct Task* current_task = list_head;
	struct Task* prev_task = NULL;
	while(current_task != NULL) {
		if(current_task->wait_time != 0)
			current_task->wait_time--;
		else {
			setTask(current_task->task_handler);
			if(prev_task == NULL)
				list_head = current_task->next_task;
			else 
				prev_task->next_task = current_task->next_task;		
			free(current_task);	
		}
		prev_task = current_task;
		current_task = current_task->next_task;
	}
}

inline void initMainQueue(){
	uint8_t index = 0;
	do{
		main_task_queue[index] = Idle;
		index++;
	} while (index != TASK_QUEUE_SIZE + 1);
}

inline void initSystem(){
	initRTOS();
	InitTasklist();
	initMainQueue();
}

void ledOff();
void ledON();

void ledOff1();
void ledON1();

void ledON(){
	PORTA |= 2;
	Task *task = createTask(ledOff, 500);
	addTaskToTasklist(task);
}

void ledOff(){
	PORTA &= ~2;
	Task *task = createTask(ledON, 500);
	addTaskToTasklist(task);
}

void ledOff1(){
	PORTA &= ~1; 
	Task *task = createTask(ledON1, 200);
	addTaskToTasklist(task);
}

void ledON1(){
	PORTA |= 1;
	Task *task = createTask(ledOff1, 200);
	addTaskToTasklist(task);
}

int main() {
	initSystem();
	volatile Task *task = createTask(ledON, 5);
	addTaskToTasklist(task);
	task = createTask(ledON1, 6);
	addTaskToTasklist(task);
	sei();
	DDRA |= 3;
	while(1){
		TaskDispatcher();
	}
}  
